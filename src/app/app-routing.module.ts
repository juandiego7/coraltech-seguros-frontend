import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PoliciesRoute } from './components/policies/policies.route';
import { AddPolicyRoute } from './components/add-policy/add-policy.route';
import { AddPolicyComponent } from './components/add-policy/add-policy.component';
import { ClientsRoute } from './components/clients/clients.route';
import { ClientPoliciesRoute } from './components/client-policies/client-policies.route';
import { ClientPoliciesComponent } from './components/client-policies/client-policies.component';

const RUTAS: Routes = [
  PoliciesRoute,
  AddPolicyRoute,
  ClientsRoute,
  ClientPoliciesRoute
];

const routes: Routes = [
  { path: '', redirectTo: 'seguros/polizas', pathMatch: 'full' },
  { path: 'seguros/polizas', redirectTo: 'seguros/polizas', pathMatch: 'full' },
  { path: 'seguros/agregar_poliza', redirectTo: 'seguros/agregar_poliza', pathMatch: 'full' },
  { path: 'seguros/agregar_poliza/:idPolicy', component: AddPolicyComponent },
  { path: 'seguros/clients', redirectTo: 'seguros/clients', pathMatch: 'full' },
  { path: 'seguros/agregar_polizas_cliente', redirectTo: 'seguros/agregar_polizas_cliente', pathMatch: 'full' },
  { path: 'seguros/agregar_polizas_cliente/:idClient', component: ClientPoliciesComponent },
  { path: 'seguros', children: RUTAS }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
