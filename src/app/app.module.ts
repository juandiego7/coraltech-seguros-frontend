import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DxDataGridModule, DxFormModule, DxLoadPanelModule, DxPopupModule, DxButtonModule } from 'devextreme-angular';
import { PoliciesComponent } from './components/policies/policies.component';
import { MenuComponent } from './components/menu/menu.component';
import { HttpClientModule } from '@angular/common/http';
import { DxiItemModule } from 'devextreme-angular/ui/nested/item-dxi';
import { LanguageService } from './services/language.service';
import { AddPolicyComponent } from './components/add-policy/add-policy.component';
import { LoaderPanelComponent } from './components/loader-panel/loader-panel.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ClientPoliciesComponent } from './components/client-policies/client-policies.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    PoliciesComponent,
    AddPolicyComponent,
    LoaderPanelComponent,
    ClientsComponent,
    ClientPoliciesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,
    AppRoutingModule,
    DxDataGridModule,
    DxiItemModule,
    DxFormModule,
    DxLoadPanelModule,
    DxPopupModule,
    DxButtonModule
  ],
  providers: [LanguageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
