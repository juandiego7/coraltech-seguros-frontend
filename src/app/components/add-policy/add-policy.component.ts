import { Component, OnInit } from '@angular/core';
import { PoliciesDto } from 'src/app/models/policies-dto.model';
import { HttpBaseSegurosService } from 'src/app/services/http-base-seguros.service';
import { RisksDto } from 'src/app/models/risks-dto.model';
import { CoveringsDto } from 'src/app/models/coverings-dto.model';
import { ActivatedRoute } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { LoaderPanelComponent } from '../loader-panel/loader-panel.component';

@Component({
  selector: 'app-add-policy',
  templateUrl: './add-policy.component.html',
  styleUrls: ['./add-policy.component.css']
})
export class AddPolicyComponent implements OnInit {

  policy: PoliciesDto;
  risks: RisksDto[] = [];
  coverings: CoveringsDto[] = [];
  maxRisk;
  loader: LoaderPanelComponent = new LoaderPanelComponent();

  constructor(private segurosService: HttpBaseSegurosService, private route: ActivatedRoute) {
    this.policy = new PoliciesDto();
    this.policy.idRiskNavigation = null;
    this.buttonOptionsSave.onClick = this.buttonOptionsSave.onClick.bind(this);
    this.onSelectionChangedRisk = this.onSelectionChangedRisk.bind(this);
  }

  buttonOptionsSave = {
    text: 'Guardar',
    type: 'success',
    onClick() {
      console.log('buttonOptions', this.policy);
      if (this.policy.idPolicy > 0) {
        this.updatePolicy();
      } else {
        this.savePolicy();
      }
    }
  };

  ngOnInit() {
    this.getRisks();
    this.getCoverigns();
    this.getPolicy();
  }

  // Service
  getPolicy() {
    const idPolicy = Number(this.route.snapshot.paramMap.get('idPolicy'));
    if (idPolicy > 0) {
      this.loader.setVisible(true);
      const END_POINT = this.segurosService.endpoint;
      this.segurosService.endpoint = END_POINT + 'Policies';
      this.segurosService.getById(idPolicy).subscribe(
        (response: any) => {
          this.loader.setVisible(false);
          const { isSuccess, result, message } = response;
          if (isSuccess) {
            this.policy = result;
            this.policy.idRiskNavigation = null;
          } else {
            console.log(message);
          }
        },
        (error: any) => {
          this.loader.setVisible(false);
          console.log(error);
        }
      );
      this.segurosService.endpoint = END_POINT;
    }
  }

  savePolicy() {
    this.loader.setVisible(true);
    const END_POINT = this.segurosService.endpoint;
    this.segurosService.endpoint = END_POINT + 'Policies';
    this.segurosService.add(this.policy).subscribe(
      (response: any) => {
        this.loader.setVisible(false);
        const { isSuccess, result, message } = response.body;
        if (isSuccess) {
          notify('Datos guardados correctamente.', 'success', 2000);
          this.policy = result;
        } else {
          notify('Ocurrió un error guardando los datos.', 'error', 2000);
        }
      },
      (error: any) => {
        this.loader.setVisible(false);
        notify('Ocurrió un error guardando los datos.', 'error', 2000);
      }
    );
    this.segurosService.endpoint = END_POINT;
  }

  updatePolicy() {
    this.loader.setVisible(true);
    const END_POINT = this.segurosService.endpoint;
    this.segurosService.endpoint = END_POINT + 'Policies';
    this.segurosService.update(this.segurosService.endpoint, this.policy).subscribe(
      (response: any) => {
        console.log(response);
        this.loader.setVisible(false);
        const { isSuccess, result, message } = response;
        if (isSuccess) {
          notify('Datos guardados correctamente.', 'success', 2000);
          this.policy = result;
        } else {
          console.log(message);
        }
      },
      (error: any) => {
        this.loader.setVisible(false);
        notify('Ocurrió un error guardando los datos.', 'error', 2000);
      }
    );
    this.segurosService.endpoint = END_POINT;
  }

  getRisks() {
    const END_POINT = this.segurosService.endpoint;
    this.segurosService.endpoint = END_POINT + 'Risks';
    this.segurosService.getAll().subscribe(
      (response: any) => {
        const { isSuccess, result, message } = response.body;
        if (isSuccess) {
          this.risks = result;
        } else {
          console.log(message);
        }
      },
      (error: any) => {
        console.log(error);
      }
    );
    this.segurosService.endpoint = END_POINT;
  }

  getCoverigns() {
    const END_POINT = this.segurosService.endpoint;
    this.segurosService.endpoint = END_POINT + 'Coverings';
    this.segurosService.getAll().subscribe(
      (response: any) => {
        const { isSuccess, result, message } = response.body;
        if (isSuccess) {
          this.coverings = result;
        } else {
          console.log(message);
        }
      },
      (error: any) => {
        console.log(error);
      }
    );
    this.segurosService.endpoint = END_POINT;
  }

  deleteCovering(idPolicy: number, idCovering: number) {
    this.loader.setVisible(true);
    const END_POINT = this.segurosService.endpoint;
    this.segurosService.endpoint = END_POINT + 'PoliciesCoverings/' + idPolicy + '/' + idCovering;
    this.segurosService.delete(this.segurosService.endpoint).subscribe(
      (response: any) => {
        this.loader.setVisible(false);
        const { isSuccess, result, message } = response;
        if (isSuccess) {
          notify('Registro eliminado correctamente.', 'success', 2000);
        } else {
          notify('ocurrió un error eliminando el registro.', 'error', 2000);
        }
      },
      (error: any) => {
        this.loader.setVisible(false);
        notify('ocurrió un error eliminando el registro.', 'error', 2000);
        console.log(error);
      }
    );
    this.segurosService.endpoint = END_POINT;
  }

  // Events
  onRowRemovedCovering(row) {
    if (row.data.idPolicy > 0) {
      this.deleteCovering(row.data.idPolicy, row.data.idCovering);
    }
  }

  onSelectionChangedRisk(e) {
    const name = e.selectedItem.name != null ? e.selectedItem.name.toLowerCase().trim() : '';
    if (name === 'alto') {
      this.maxRisk = 50;
    } else {
      this.maxRisk = undefined;
    }
  }

  formarter(e) {
    return e + '%';
  }

}
