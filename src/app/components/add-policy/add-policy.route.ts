import { Route } from '@angular/router';
import { AddPolicyComponent } from './add-policy.component';

export const AddPolicyRoute: Route = {
  path: 'agregar_poliza',
  component: AddPolicyComponent,
  data: {
    authorities: [],
    pageTitle: 'Agregar póliza'
  }
};
