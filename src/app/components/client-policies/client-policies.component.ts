import { Component, OnInit, ViewChild } from '@angular/core';
import { ClientsDto } from 'src/app/models/clients-dto.model';
import { PoliciesDto } from 'src/app/models/policies-dto.model';
import { LoaderPanelComponent } from '../loader-panel/loader-panel.component';
import { HttpBaseSegurosService } from 'src/app/services/http-base-seguros.service';
import { ActivatedRoute } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-client-policies',
  templateUrl: './client-policies.component.html',
  styleUrls: ['./client-policies.component.css']
})
export class ClientPoliciesComponent implements OnInit {

  client: ClientsDto;
  policies: PoliciesDto[] = [];
  loader: LoaderPanelComponent = new LoaderPanelComponent();
  isPopupAddPoliciesVisible = false;
  @ViewChild('gridPolicies')
  gridPolicies: DxDataGridComponent;

  constructor(private segurosService: HttpBaseSegurosService, private route: ActivatedRoute) {
    this.client = new ClientsDto();
    this.buttonOptionsSave.onClick = this.buttonOptionsSave.onClick.bind(this);
    this.onClickUpdatePolicy = this.onClickUpdatePolicy.bind(this);
  }

  buttonOptionsSave = {
    text: 'Guardar',
    type: 'success',
    onClick() {
      console.log(this.client);
      this.updateClient();
    }
  };

  ngOnInit() {
    this.getClient();
    this.getPolicies();
  }

  // Service
  getClient() {
    const idClient = Number(this.route.snapshot.paramMap.get('idClient'));
    if (idClient > 0) {
      this.loader.setVisible(true);
      const END_POINT = this.segurosService.endpoint;
      this.segurosService.endpoint = END_POINT + 'Clients';
      this.segurosService.getById(idClient).subscribe(
        (response: any) => {
          this.loader.setVisible(false);
          const { isSuccess, result, message } = response;
          if (isSuccess) {
            this.client = result;
          } else {
            notify('Ocurrió un error consultando los datos.', 'error', 2000);
          }
        },
        (error: any) => {
          this.loader.setVisible(false);
          notify('Ocurrió un error consultando los datos.', 'error', 2000);
        }
      );
      this.segurosService.endpoint = END_POINT;
    }
  }

  getClientUpdated(text) {
    const idClient = Number(this.route.snapshot.paramMap.get('idClient'));
    if (idClient > 0) {
      this.loader.setVisible(true);
      const END_POINT = this.segurosService.endpoint;
      this.segurosService.endpoint = END_POINT + 'Clients';
      this.segurosService.getById(idClient).subscribe(
        (response: any) => {
          this.loader.setVisible(false);
          const { isSuccess, result, message } = response;
          if (isSuccess) {
            this.client = result;
            notify('La póliza se ' + text + ' correctamente.', 'success', 2000);
          } else {
            notify('Ocurrió un error procesando la solicitud.', 'error', 2000);
          }
        },
        (error: any) => {
          this.loader.setVisible(false);
          notify('Ocurrió un error procesando la solicitud.', 'error', 2000);
        }
      );
      this.segurosService.endpoint = END_POINT;
    }
  }

  updateClient() {
    this.loader.setVisible(true);
    const END_POINT = this.segurosService.endpoint;
    this.segurosService.endpoint = END_POINT + 'Clients';
    this.segurosService.update(this.segurosService.endpoint, this.client).subscribe(
      (response: any) => {
        this.loader.setVisible(false);
        const { isSuccess, result, message } = response;
        if (isSuccess) {
          this.client = result;
          notify('Los datos se guardaron correctamente.', 'success', 2000);
        } else {
          notify('Ocurrió un error guardando los datos.', 'error', 2000);
        }
      },
      (error: any) => {
        this.loader.setVisible(false);
        notify('Ocurrió un error guardando los datos.', 'error', 2000);
      }
    );
    this.segurosService.endpoint = END_POINT;
  }

  updateClientPolicy(clientPolicy) {
    this.loader.setVisible(true);
    const text = clientPolicy.active ? 'activó' : 'canceló';
    const END_POINT = this.segurosService.endpoint;
    this.segurosService.endpoint = END_POINT + 'ClientsPolicies';
    this.segurosService.update(this.segurosService.endpoint, clientPolicy).subscribe(
      (response: any) => {
        this.loader.setVisible(false);
        const { isSuccess, result, message } = response;
        if (isSuccess) {
          this.getClientUpdated(text);
        } else {
          notify('Ocurrió un error procesando la solicitud.', 'error', 2000);
        }
      },
      (error: any) => {
        this.loader.setVisible(false);
        notify('Ocurrió un error procesando la solicitud.', 'error', 2000);
      }
    );
    this.segurosService.endpoint = END_POINT;
  }

  getPolicies() {
    this.loader.setVisible(true);
    const END_POINT = this.segurosService.endpoint;
    this.segurosService.endpoint = END_POINT + 'Policies';
    this.segurosService.getAll().subscribe(
      (response: any) => {
        this.loader.setVisible(false);
        const { isSuccess, result, message } = response.body;
        if (isSuccess) {
          this.policies = result;
        } else {
          notify('Ocurrió un error consultando los datos.', 'error', 2000);
        }
      },
      (error: any) => {
        this.loader.setVisible(false);
        notify('Ocurrió un error consultando los datos.', 'error', 2000);
      }
    );
    this.segurosService.endpoint = END_POINT;
  }

  // Functions
  addPolicies() {
    this.gridPolicies.instance.getSelectedRowsData().forEach(x => {
      const idPolicy = this.client.clientsPolicies.find(i => i.idPolicy === x.idPolicy);
      if (idPolicy !== undefined) { return; }
      const clientPolicy = {
        idClient: 0,
        idPolicy: x.idPolicy,
        active: true,
        idPolicyNavigation: x
      };
      this.client.clientsPolicies.push(clientPolicy);
    });
    this.gridPolicies.instance.clearSelection();
    this.isPopupAddPoliciesVisible = false;
  }

  // Events
  onClickUpdatePolicy(e) {
    e.row.data.active = !e.row.data.active;
    this.updateClientPolicy(e.row.data);
  }

  onToolbarPreparingPoliciesClient(e) {
    e.toolbarOptions.items.forEach(item => {
      if (item.name === 'searchPanel') {
        item.location = 'before';
      }
    });
    e.toolbarOptions.items.unshift({
      location: 'after',
      widget: 'dxButton',
      options: {
        hint: 'Agregar',
        text: 'Agregar póliza',
        icon: 'add',
        onClick: (d) => { this.isPopupAddPoliciesVisible = true; }
      },
      showText: 'always'
    });
  }

  onToolbarPreparingPolicies(e) {
    e.toolbarOptions.items.forEach(item => {
      if (item.name === 'searchPanel') {
        item.location = 'before';
      }
    });
  }

  calculateDisplayValueActive(row) {
    return row.active ? 'Activa' : 'Cancelada';
  }

  isActive(e) {
    return !e.row.data.active;
  }
  isCancel(e) {
    return e.row.data.active;
  }
}
