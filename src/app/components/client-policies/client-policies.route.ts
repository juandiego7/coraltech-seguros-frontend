import { Route } from '@angular/router';
import { ClientPoliciesComponent } from './client-policies.component';

export const ClientPoliciesRoute: Route = {
  path: 'agregar_polizas_cliente',
  component: ClientPoliciesComponent,
  data: {
    authorities: [],
    pageTitle: 'Agregar pólizas a cliente'
  }
};
