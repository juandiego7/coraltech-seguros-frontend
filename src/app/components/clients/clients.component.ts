import { Component, OnInit } from '@angular/core';
import { ClientsDto } from 'src/app/models/clients-dto.model';
import { LoaderPanelComponent } from '../loader-panel/loader-panel.component';
import { HttpBaseSegurosService } from 'src/app/services/http-base-seguros.service';
import notify from 'devextreme/ui/notify';
import { Router } from '@angular/router';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  clients: ClientsDto[] = [];
  loader: LoaderPanelComponent = new LoaderPanelComponent();

  constructor(private segurosService: HttpBaseSegurosService, private router: Router) {
    this.onClickShowPolicies = this.onClickShowPolicies.bind(this);
  }

  ngOnInit() {
    this.getClients();
  }

  // Services
  getClients() {
    this.loader.setVisible(true);
    const END_POINT = this.segurosService.endpoint;
    this.segurosService.endpoint = END_POINT + 'Clients';
    this.segurosService.getAll().subscribe(
      (response: any) => {
        this.loader.setVisible(false);
        const { isSuccess, result, message } = response.body;
        if (isSuccess) {
          this.clients = result;
        } else {
          notify('Ocurrió un error consultando los datos.', 'error', 2000);
        }
      },
      (error: any) => {
        this.loader.setVisible(false);
        notify('Ocurrió un error consultando los datos.', 'error', 2000);
      }
    );
    this.segurosService.endpoint = END_POINT;
  }

  // Eventos
  onClickShowPolicies(e) {
    console.log(e.row.data.idClient);
    this.router.navigate(['seguros/agregar_polizas_cliente/' + e.row.data.idClient]);
  }

  onToolbarPreparingClients(e) {
    e.toolbarOptions.items.forEach(item => {
      if (item.name === 'searchPanel') {
        item.location = 'before';
      }
    });
  }

}
