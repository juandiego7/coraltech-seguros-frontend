import { Route } from '@angular/router';
import { ClientsComponent } from './clients.component';

export const ClientsRoute: Route = {
  path: 'clients',
  component: ClientsComponent,
  data: {
    authorities: [],
    pageTitle: 'Clientes'
  }
};
