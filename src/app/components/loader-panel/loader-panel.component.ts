import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader-panel',
  templateUrl: './loader-panel.component.html',
  styleUrls: ['./loader-panel.component.css']
})
export class LoaderPanelComponent implements OnInit {

  loadingVisible = false;

  constructor() { }

  ngOnInit() {
  }

  public setVisible(isVisible) {
    const loader: HTMLElement = document.querySelector('.t-loader');
    loader.style.visibility = isVisible ? 'visible' : 'hidden';
  }

}
