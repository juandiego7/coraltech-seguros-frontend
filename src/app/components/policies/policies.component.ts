import { Component, OnInit, ViewChild } from '@angular/core';
import { PoliciesDto } from 'src/app/models/policies-dto.model';
import { HttpBaseSegurosService } from 'src/app/services/http-base-seguros.service';
import { RisksDto } from 'src/app/models/risks-dto.model';
import DataSource from 'devextreme/data/data_source';
import ArrayStore from 'devextreme/data/array_store';
import { PoliciesCoveringsDto } from 'src/app/models/policies-coverings-dto.model';
import { Router } from '@angular/router';
import { LoaderPanelComponent } from '../loader-panel/loader-panel.component';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'app-policies',
  templateUrl: './policies.component.html',
  styleUrls: ['./policies.component.css']
})
export class PoliciesComponent implements OnInit {

  policies: PoliciesDto[] = [];
  risks: RisksDto[] = [];
  policiesCoverigns: PoliciesCoveringsDto[];
  loadingVisible = false;
  loader: LoaderPanelComponent = new LoaderPanelComponent();

  constructor(private segurosService: HttpBaseSegurosService, private router: Router) {
  }

  ngOnInit() {
    this.getPolicies();
    this.getRisks();
  }

  // Services
  getPolicies() {
    this.loader.setVisible(true);
    const END_POINT = this.segurosService.endpoint;
    this.segurosService.endpoint = END_POINT + 'Policies';
    this.segurosService.getAll().subscribe(
      (response: any) => {
        this.loader.setVisible(false);
        const { isSuccess, result, message } = response.body;
        if (isSuccess) {
          this.policies = result;
        } else {
          notify('Ocurrió un error consultando los datos.', 'error', 2000);
        }
      },
      (error: any) => {
        this.loader.setVisible(false);
        notify('Ocurrió un error consultando los datos.', 'error', 2000);
      }
    );
    this.segurosService.endpoint = END_POINT;
  }

  getRisks() {
    const END_POINT = this.segurosService.endpoint;
    this.segurosService.endpoint = END_POINT + 'Risks';
    this.segurosService.getAll().subscribe(
      (response: any) => {
        const { isSuccess, result, message } = response.body;
        if (isSuccess) {
          this.risks = result;
        } else {
          console.log(message);
        }
      },
      (error: any) => {
        console.log(error);
      }
    );
    this.segurosService.endpoint = END_POINT;
  }

  deletePolicy(idPolicy: number) {
    this.loader.setVisible(true);
    const END_POINT = this.segurosService.endpoint;
    this.segurosService.endpoint = END_POINT + 'Policies/' + idPolicy;
    this.segurosService.delete(this.segurosService.endpoint).subscribe(
      (response: any) => {
        this.loader.setVisible(false);
        const { isSuccess, result, message } = response;
        if (isSuccess) {
          notify('Registro eliminado correctamente.', 'success', 2000);
        } else {
          notify('ocurrió un error eliminando el registro.', 'error', 2000);
        }
      },
      (error: any) => {
        this.loader.setVisible(false);
        notify('ocurrió un error eliminando el registro.', 'error', 2000);
      }
    );
    this.segurosService.endpoint = END_POINT;
  }

  // Functions
  getCoverings(key) {
    const policy = this.policies.find(x => x.idPolicy === key);
    return policy !== undefined ? policy.policiesCoverings : [];
  }

  // Eventos
  onInitNewRowPolicy(policy) {
    this.router.navigate(['seguros/agregar_poliza']);
  }

  onRowRemovedPolicy(row) {
    this.deletePolicy(row.data.idPolicy);

  }

  onEditingStartPolicy(e) {
    this.router.navigate(['seguros/agregar_poliza/' + e.data.idPolicy]);
  }

  calculateDisplayValueCoverage(row) {
    return row.coverage != null ? row.coverage + '%' : '';
  }

  onToolbarPreparingPolicies(e) {
    e.toolbarOptions.items.forEach(item => {
      if (item.name === 'searchPanel') {
        item.location = 'before';
      }
    });
  }

}
