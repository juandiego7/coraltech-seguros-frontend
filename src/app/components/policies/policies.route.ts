import { Route } from '@angular/router';
import { PoliciesComponent } from './policies.component';

export const PoliciesRoute: Route = {
  path: 'polizas',
  component: PoliciesComponent,
  data: {
    authorities: [],
    pageTitle: 'Pólizas'
  }
};
