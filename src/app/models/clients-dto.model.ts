import { PoliciesDto } from './policies-dto.model';
import { ClientsPoliciesDto } from './clients-policies-dto.model';

export class ClientsDto {
  idClient: number;
  firstName?: string;
  lastName?: string;
  document?: string;

  clientsPolicies: ClientsPoliciesDto[];

  constructor() {
    this.idClient = 0;
    this.firstName = '';
    this.lastName = '';
    this.document = '';

    this.clientsPolicies = [];
  }
}
