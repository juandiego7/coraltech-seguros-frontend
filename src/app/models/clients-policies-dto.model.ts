import { ClientsDto } from './clients-dto.model';

import { PoliciesDto } from './policies-dto.model';

export class ClientsPoliciesDto {
  idClient?: number;
  idPolicy?: number;
  active?: boolean;

  idClientNavigation?: ClientsDto;
  idPolicyNavigation?: PoliciesDto;

  constructor() {
    this.idClient = 0;
    this.idPolicy = 0;
    this.active = null;

    this.idClientNavigation = new ClientsDto();
    this.idPolicyNavigation = new PoliciesDto();
  }
}
