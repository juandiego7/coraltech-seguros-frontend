import { PoliciesCoveringsDto } from './policies-coverings-dto.model';

export class CoveringsDto {
  idCovering: number;
  name?: string;

  policiesCoverings?: PoliciesCoveringsDto[];

  constructor() {
    this.idCovering = 0;
    this.name = null;

    this.policiesCoverings = [];
  }
}
