import { CoveringsDto } from './coverings-dto.model';

import { PoliciesDto } from './policies-dto.model';

export class PoliciesCoveringsDto {
  idPolicy?: number;
  idCovering?: number;
  coverage?: number;

  idCoveringNavigation?: CoveringsDto;
  idPolicyNavigation?: PoliciesDto;

  constructor() {
    this.idPolicy = 0;
    this.idCovering = 0;
    this.coverage = null;

    this.idCoveringNavigation = new CoveringsDto();
    this.idPolicyNavigation = new PoliciesDto();
  }

}
