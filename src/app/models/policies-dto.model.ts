import { RisksDto } from './risks-dto.model';

import { PoliciesCoveringsDto } from './policies-coverings-dto.model';

export class PoliciesDto {
  idPolicy: number;
  name?: string;
  description?: string;
  startValidity?: Date;
  periodCoverage?: number;
  price?: number;
  idRisk?: number;

  idRiskNavigation: RisksDto;
  policiesCoverings: PoliciesCoveringsDto[];

  constructor() {
   this.idPolicy = 0;
   this.name = '';
   this.description = '';
   this.startValidity = null;
   this.periodCoverage = null;
   this.price = null;
   this.idRisk = null;

   this.idRiskNavigation = new RisksDto();
   this.policiesCoverings = [];
  }

}
