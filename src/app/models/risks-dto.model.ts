import { PoliciesDto } from './policies-dto.model';

export class RisksDto {
  idRisk: number;
  name?: string;

  policies?: PoliciesDto[];

  constructor() {
    this.idRisk = 0;
    this.name = '';

    this.policies = [];
  }
}
