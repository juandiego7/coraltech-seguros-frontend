import { TestBed } from '@angular/core/testing';

import { HttpBaseSegurosService } from './http-base-seguros.service';

describe('HttpBaseSegurosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpBaseSegurosService = TestBed.get(HttpBaseSegurosService);
    expect(service).toBeTruthy();
  });
});
