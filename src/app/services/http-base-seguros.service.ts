import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpBaseSegurosService {

  private headers = new HttpHeaders();
  public endpoint = 'https://localhost:44312/api/v1/';

  constructor(private httpClient: HttpClient) {
    this.headers = this.headers.set('Content-Type', 'application/json');
    this.headers = this.headers.set('Accept', 'application/json');
  }

  getAll<T>() {
    const baseURL = `${this.endpoint}`;
    return this.httpClient.get<T>(baseURL, { observe: 'response' });
  }

  getById<T>(id: number) {
    return this.httpClient.get<T>(`${this.endpoint}/${id}`);
  }

  getEntities<T>(id: number, value: string) {
    return this.httpClient.get<T>(`${this.endpoint}/${id}/${value}`);
  }

  add<T>(toAdd: T) {
    return this.httpClient.post<T>(this.endpoint, toAdd, { headers: this.headers });
  }

  update<T>(url: string, toUpdate: T) {
    return this.httpClient.put<T>(url, toUpdate, { headers: this.headers });
  }

  delete<T>(url: string) {
    return this.httpClient.delete(url);
  }
}

